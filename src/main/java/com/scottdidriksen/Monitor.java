package com.scottdidriksen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import org.json.simple.JSONObject;

/**
 * Monitor class implemented by Scott Didriksen
 * This class is where all of the data work is done. 
 */
public class Monitor
{
    //<timestamp>|<Satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>

    File satfile = null;
    String[] satVals = new String[7];
    LocalDateTime sattime = null;
    Integer satnum  = 0;
    double redhigh = 0.0;
    double yellowhigh = 0.0;
    double yellowlow = 0.0;
    double redlow = 0.0;
    double rawval = 0.0;
    String satcomponent = "";

    /**
     * satdata is somewhat complex. It first separates out data for each satellite then breaks out by type of data. Finally it stores the
     * localdatetime of the bad reading based on severity. Each time a new item is read it looks through the component has and looks for localdatetimes for items that are old and
     * deletes them. It then adds the new item and determines if there are more than two for that component within the last 5 minutes. If so, a message is outputin json with the error.
     * If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
     * If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
     * The ealiest time for the enties is used for output
     */
    HashMap<Integer, HashMap<String, HashMap<String, ArrayList<LocalDateTime>>>> satdata = new HashMap();
    public void startMonitor(File infile)
    {
        satfile = infile;
        String line = "";
        try
        {
            FileReader fr = new FileReader(satfile);
            BufferedReader br=new BufferedReader(fr);
            while((line = br.readLine()) !=null)
            {
                readData(line);
                parseData();
                evalData();
            }
        }
        catch (Exception e)
        {
            System.out.println(" ERROR! " + e.getMessage());
        }
    }

    /**
     * evalData checks if the readings are out of spec. If they are they create an entry in the Hash. I originally
     * had the checks for yellow readings but commented them out.
     */
    public void evalData()
    {
        if(satcomponent.equals("BATT"))
        {
            if(rawval < redlow)
            {
                createEntry(satnum, "RED LOW", satcomponent, sattime);
            }
            else if(rawval < yellowlow)
            {
               // createEntry(satnum, "YELLOW LOW", satcomponent, sattime);
            }
        }
        else if(satcomponent.equals("TSTAT"))
        {
            if(rawval > redhigh)
            {
                createEntry(satnum, "RED HIGH", satcomponent, sattime);
            }
            else if(rawval > yellowhigh)
            {
               // createEntry(satnum, "YELLOW HIGH", satcomponent, sattime);
            }
        }
    }

    /**
     * createEntry is the most complex part of this program. It takes the parsed data and adds it to the hash, creating
     * major entries if absent as we go. I did not hard code these catagories in an array as that was a bit simplistic.
     * @param satNum
     * @param severity
     * @param comp
     * @param stime
     */
    private void createEntry(Integer satNum, String severity, String comp, LocalDateTime stime)
    {
        //    HashMap<Integer, HashMap<String, HashMap<String, HashMap<String, ArrayList<LocalDateTime>>> satdata = new HashMap<Integer, HashMap<String, HashMap<String, LocalDateTime>>>();
        //First data on satellite
        HashMap<String, ArrayList<LocalDateTime>> tmpHTime = new HashMap<>(); //Severity,time
        HashMap<String, HashMap<String, ArrayList<LocalDateTime>>> satHash = null;  //One satellite comp, hash
        HashMap<String, ArrayList<LocalDateTime>>  sevHash = null; // All components
        ArrayList<LocalDateTime> timeArray = null;
        if(! satdata.containsKey(satnum))
        {
            satHash = new HashMap<>();
            satdata.put(satNum, satHash);
            //Next components
        }
        else
        {
            satHash = satdata.get(satNum);
        }
        if(! satHash.containsKey(comp))
        {
            sevHash = new HashMap<>();
            satHash.put(comp, sevHash);
        }
        else
        {
            sevHash = satHash.get(comp);
        }

        if(! sevHash.containsKey(severity))
        {
            timeArray = new ArrayList<>();
            sevHash.put(severity, timeArray);
        }
        else
        {
            timeArray = sevHash.get(severity);
        }

        timeArray.add(stime);
        LocalDateTime mtime = stime.minus(5, ChronoUnit.MINUTES);
        ListIterator<LocalDateTime> iter = timeArray.listIterator();
        while(iter.hasNext())
        {
            LocalDateTime ttime = iter.next();
            if(ttime.isBefore(mtime))
            {
                iter.remove();
            }
        }
        if(timeArray.size()>2)
        {
            reportError(satnum, severity, comp);
        }
     }

    private void reportError(Integer satNum, String severity, String comp)
    {
        JSONObject obj = new JSONObject();
        obj.put("satelliteId", satNum);
        obj.put("severity", severity);
        obj.put("component", comp);
        HashMap tmpSat = satdata.get(satNum);
        HashMap tmpComp = (HashMap) tmpSat.get(comp);
        ArrayList tmpSev = (ArrayList) tmpComp.get(severity);
        LocalDateTime tmpTime = (LocalDateTime) tmpSev.get(0);
        if(tmpSev.size()>1)
        {
            for(int i = 1;i < tmpSev.size(); i++)
            {
                if(tmpTime.isAfter((LocalDateTime)tmpSev.get(i)))
                {
                    tmpTime = (LocalDateTime)tmpSev.get(i);
                }
            }
        }
        obj.put("timestamp", tmpTime + "Z");
        System.out.println(obj);
    }

    public void readData(String sdata)
    {
        satVals = sdata.split("\\|");
    }

    public void parseData()
    {
        String[] tmpdt = satVals[SatConstants.TStamp].split(" ");
        String year = tmpdt[0].substring(0,4);
        String month = tmpdt[0].substring(4,6);
        String day = tmpdt[0].substring(6);
        String[] dtTime = tmpdt[1].split(":");
        sattime = LocalDateTime.of(LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day)), LocalTime.parse(tmpdt[1]));
        satnum = Integer.parseInt(satVals[SatConstants.SATNUM]);
        redhigh = Double.parseDouble(satVals[SatConstants.REDHIGH]);
        yellowhigh = Double.parseDouble(satVals[SatConstants.YELLOWHIGH]);
        yellowlow = Double.parseDouble(satVals[SatConstants.YELLOWLOW]);
        redlow = Double.parseDouble(satVals[SatConstants.REDLOW]);
        rawval = Double.parseDouble(satVals[SatConstants.RAWVALUE]);
        satcomponent = satVals[SatConstants.COMPONENT];
    }

    public String[] getSatVals()
    {
        return satVals;
    }

    public LocalDateTime getSattime()
    {
        return sattime;
    }

    public int getSatnum()
    {
        return satnum;
    }

    public double getRedhigh()
    {
        return redhigh;
    }

    public double getYellowhigh()
    {
        return yellowhigh;
    }

    public double getYellowlow()
    {
        return yellowlow;
    }

    public double getRedlow()
    {
        return redlow;
    }

    public double getRawval()
    {
        return rawval;
    }

    public HashMap<Integer, HashMap<String, HashMap<String, ArrayList<LocalDateTime>>>> getSatdata()
    {
        return satdata;
    }
}
