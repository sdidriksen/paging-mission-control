package com.scottdidriksen;

public class SatConstants
{
    final static public int TStamp = 0;
    final static public int SATNUM = 1;
    final static public int REDHIGH = 2;
    final static public int YELLOWHIGH = 3;
    final static public int YELLOWLOW = 4;
    final static public int REDLOW = 5;
    final static public int RAWVALUE = 6;
    final static public int COMPONENT = 7;

}
