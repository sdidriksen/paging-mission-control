package com.scottdidriksen;

import java.io.File;

/**
 * Solution by Scott Didriksen
 * Main class calls the monitor class to execute. This made writing tests much easier.
 * Main expects the name of the satellite data file as an input.
 * Expected format: <timestamp>|<Satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
 */
public class Satellite
{
    public static void main(String[] args)
    {
        File inFile = null;
        if (0 < args.length) {
            inFile = new File(args[0]);
        } else {
            System.err.println("Invalid arguments count:" + args.length);
            System.exit(1);
        }
        Monitor mon = new Monitor();
        mon.startMonitor(inFile);
    }

}
