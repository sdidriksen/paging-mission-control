package com.scottdidriksen;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * These tests are simple and obvioulsly not exhaustive.
 */
public class MonitorImpl extends Monitor
{
    Monitor tmon = new Monitor();
    String testinput = "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT";
    @Test
    @DisplayName("testParse")
    public void testParse()
    {
        tmon.readData(testinput);
        tmon.parseData();
        String tstString = tmon.getSatnum() +","+ tmon.getRedhigh()+","+ tmon.getRawval();
        assertEquals(tstString, "1000,101.0,102.9");
    }
    @Test
    @DisplayName("testEvalData")
    public void testEvalData()
    {
        tmon.readData(testinput);
        tmon.parseData();
        tmon.evalData();
        assertTrue(tmon.getSatdata().get(1000)!=null);
    }

}
